document.getElementById('confirmpass').addEventListener('keyup', confirmPassword);

function confirmPassword() {
    let password = document.getElementById('password');
    let confirmation = document.getElementById('confirmpass');
    let confirmationLabel = document.getElementsByClassName('confirm-label')[0];
    let button = document.getElementsByClassName('submit-button');

    if (password.value !== confirmation.value) {
        confirmationLabel.style.color = 'rgba(229, 116, 72, 1)';
        confirmationLabel.innerText = 'PASSWORDS DO NOT MATCH!'
        confirmation.style.backgroundColor = 'rgba(229, 116, 72, 0.2)';
        button[0].disabled = true;
    } else {
        confirmationLabel.style.color = 'rgba(96, 136, 63, 1)';
        confirmationLabel.innerText = 'THE PASSWORDS MATCH'
        confirmation.style.backgroundColor = 'rgba(96, 136, 63, 0.2)';
        button[0].disabled = false;
    }
}